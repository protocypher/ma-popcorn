# Popcorn

**Movie Store**

Popcorn is a basic, text based, movie store application that uses the `cmd.Cmd`
framework. It manages an inventory of movies that can be rented and returned.
It also produces reports of individuals to call for overdue movies and which
users need to pay a late fee for their next rental.

