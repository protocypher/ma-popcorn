from setuptools import setup


setup(
      name="Popcorn",
      version="0.1.0",
      packages=["popcorn"],
      url="https://bitbucket.org/protocypher/ma-popcorn",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A move rental application."
)

